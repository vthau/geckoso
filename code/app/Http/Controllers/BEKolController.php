<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use \Session;

use App\Api\Core;

use App\User;

use Maatwebsite\Excel\Facades\Excel;
use App\Excel\ExportClient;

use App\Model\Log;
use App\Model\UserLevel;
use App\Model\File;
use App\Model\Kol;
use App\Model\UserCategory;
use App\Model\UserCategoryOther;


class BEKolController extends Controller
{
    protected $_apiCore = null;
    protected $_viewer = null;

    public function __construct()
    {
        $this->_apiCore = new Core();

        $this->middleware(function ($request, $next) {
            $this->_viewer = $this->_apiCore->getViewer();

            //
            if (
                $this->_viewer &&
                ($this->_viewer->isDeleted() || $this->_viewer->isBlocked() || !$this->_viewer->isStaff())
            ) {
                return redirect('/invalid');
            }

            return $next($request);
        });

        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $params = $request->all();

        $values = [
            'page_title' => 'Danh Sách Kol',

            'params' => $params,
        ];

        $values['items'] = Kol::paginate(20);

        //message
        $message = (Session::get('MESSAGE'));
        if (!empty($message)) {
            Session::forget('MESSAGE');
        }
        $values['message'] = $message;

        return view("pages.back_end.kol.index", $values);
    }

    public function add(Request $request)
    {

        $params = $request->all();
        $pageTitle = 'Tạo Kol';
        $itemId = (isset($params['id'])) ? (int)$params['id'] : 0;

        $user = Kol::find($itemId);
        if ($user) {
            $pageTitle = 'Sửa Thông Tin Kol';
        }

        $values = [
            'page_title' => $pageTitle,

            'user' => $user,
        ];

        return view("pages.back_end.kol.add", $values);
    }

    public function save(Request $request)
    {
        if (!count($request->post())) {
            return redirect('/admin/clients');
        }
        $values = $request->post();
        $itemId = (isset($values['item_id'])) ? (int)$values['item_id'] : 0;

        unset($values['_token']);


        $kol = Kol::find($itemId);
        if ($kol) {
            $kol->update($values);
        } else {
            $kol = Kol::create($values);
            Session::put('MESSAGE', 'ITEM_ADDED');
        }

        return redirect('/admin/kols');
    }

    public function delete(Request $request)
    {
        $values = $request->post();
        $itemId = (isset($values['item_id'])) ? (int)$values['item_id'] : 0;
        Kol::find($itemId)->delete();
        return response()->json([]);
    }
}

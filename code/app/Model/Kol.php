<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use App\Api\Core;
use App\Api\FE;
use App\User;
use \Session;
use \Artisan;

class Kol extends Item
{
    public $table = 'kols';

    protected $fillable = [
        'name', 'role', 'star', 'note',
    ];

    public function kols()
    {
        return $this->belongsToMany(Product::class, 'kol_product', 'kol_id', 'product_id');
    }
}

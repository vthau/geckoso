<?php
$apiCore = new \App\Api\Core;

if (!isset($kol)) {
    return '';
}

?>

<div class="review_item" data-id="{{$kol->id}}">
    <div class="row">
        <div class="col-md-8 mb__10 star_wrapper">
            <div class="rating-group mr__10">
                @for($i=1;$i<=5;$i++)
                    @if($i > $kol->star)
                        <label class="rating__label"><i class="rating__icon rating__icon--none fa fa-star"></i></label>
                    @else
                        <label class="rating__label"><i class="rating__icon rating__icon--star fa fa-star"></i></label>
                    @endif
                @endfor
            </div>

            <div class="rating-group">
                <b class="text-capitalize">{{$kol->name}} -  {{ $kol->role}}</b>
            </div>
        </div>
        <div class="col-md-4 mb__10 text-right">
            <span class="fs-11">{{$apiCore->timeToString($kol->created_at)}}</span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 mb__10">
            <?php echo nl2br($kol->note);?>
        </div>
    </div>
</div>

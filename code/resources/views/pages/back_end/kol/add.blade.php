<?php
$pageTitle = (isset($page_title)) ? $page_title : "";
$activePage = (isset($active_page)) ? $active_page : "";

$apiCore = new \App\Api\Core();
$viewer = $apiCore->getViewer();

$apiFE = new \App\Api\FE;
?>

@extends('templates.be.master')

@section('content')

    <div>
        <div class="fade-in">
            <form action="{{url('admin/kol/save')}}" method="post" enctype="multipart/form-data"
                  id="frm-add" accept-charset="UTF-8" autocomplete="off"
            >
                @csrf

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong>{{$pageTitle}}</strong>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            @if ($user && $user->name)
                                            <div class="alert alert-info">Sửa Thông Tin Kol: {{$user->name}}</div>
                                            @else
                                            <div class="alert alert-info">Thêm Kol</div>
                                            @endif

                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-3" id="req-name">
                                        <div class="form-group">
                                            <label class="frm-label required">* Họ Tên</label>
                                            <input required value="{{$user ? $user->name : ""}}" name="name" type="text" autocomplete="off" class="form-control" />
                                        </div>

                                        <div class="form-group alert alert-danger hidden">Vui lòng nhập họ tên.</div>
                                    </div>

                                    <div class="col-md-3" id="req-role">
                                        <div class="form-group">
                                            <label class="frm-label required">* Chức vụ</label>
                                            <input required value="{{$user ? $user->role : ""}}" name="role" type="text" autocomplete="off" class="form-control" />
                                        </div>

                                        <div class="form-group alert alert-danger hidden">Vui lòng nhập chức vụ.</div>
                                    </div>

                                    <div class="col-md-3" id="req-star">
                                        <div class="form-group">
                                            <label class="frm-label required">* Số sao</label>
                                            <input required value="{{$user ? $user->star : ""}}" name="star" type="number" autocomplete="off" class="form-control" />
                                        </div>

                                        <div class="form-group alert alert-danger hidden">Số phải từ 1 đến 5</div>
                                    </div>

                                </div>


                                <div class="row">

                                <div class="col-12" id="req-note">
                                    <div class="form-group">
                                        <label class="frm-label required">* Đánh giá</label>
                                        <textarea required name="note" rows="5" class="form-control">{{$user ? $user->note : ""}}</textarea>
                                    </div>

                                    <div class="form-group alert alert-danger hidden">Vui lòng nhập đánh giá.</div>
                                </div>

                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm mb-1">
                                    <i class="fa fa-check-circle mr-1"></i>
                                    Xác Nhận
                                </button>

                                <input type="hidden" name="item_id" value="{{$user ? $user->id : ""}}" />
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript" src="{{url('public/js/back_end/kol_add.js')}}"></script>

@stop

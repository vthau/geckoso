<?php
$pageTitle = (isset($page_title)) ? $page_title : "";
$activePage = (isset($active_page)) ? $active_page : "";

$apiCore = new \App\Api\Core();

$viewer = $apiCore->getViewer();
?>

@extends('templates.be.master')

@section('content')

<style type="text/css">
    .frm-search .form-group>div {
        float: left;
    }
</style>

<div>
    <div class="fade-in">
        <div class="row">
            <div class="col-md-12">
                <div class="btn-menu">
                    <button class="btn btn-primary btn-sm mb-1" onclick="openPage('{{url('admin/kol/add')}}')">
                        <i class="fa fa-plus-circle mr-1"></i>
                        Tạo Đánh Giá
                    </button>
                </div>



                @if (count($items))
                <div class="card">
                    <div class="card-header">
                        <strong>{{$pageTitle}}</strong>

                        <div class="c-header-right font-weight-bold">
                            Tổng Cộng: {{$items->total()}}
                        </div>
                    </div>

                    <div class="card-body">
                        <table class="table table-responsive-sm table-striped">
                            <thead>
                                <tr>
                                    <th>Tên</th>
                                    <th>Chức vụ</th>
                                    <th>Số sao</th>
                                    <th>Nội dung</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php foreach ($items as $user) :

                                ?>
                                    <tr id="item-{{$user->id}}">
                                        <td class="text-capitalize">{{$user->name}}</td>
                                        <td class="text-capitalize">{{$user->role}}</td>
                                        <td class="text-capitalize">{{$user->star}}</td>
                                        <td class="text-capitalize">{{$user->note}}</td>

                                        <td>
                                            <div class="align-right">

                                                <button class="btn btn-info btn-sm mb-1" title="Sửa" data-original-title="Sửa"  onclick="openPage('{{url('admin/kol/add?id=' . $user->id)}}')">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                <button class="btn btn-danger btn-sm mb-1" title="Xóa" data-original-title="Xóa" onclick="deleteItem({{$user->id}})">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="gks_pagination">
                    {{$items->appends(request()->query())->links()}}
                </div>
                @else
                <div class="clearfix mb-4 mt-4">
                    <span class="alert alert-info notfound"></span>
                </div>
                @endif

            </div>
        </div>
    </div>
</div>

<!-- <script type="text/javascript" src="{{url('public/js/back_end/clients.js')}}"></script> -->

<script>
    function deleteItem(id) {
        console.log(id);
        jQuery.ajax({
            url: gks.baseURL + "/admin/kol/delete",
            type: "post",
            data: {
                item_id: id,
                _token: gks.tempTK,
            },
            success: function (response) {
                reloadPage();
            },
    });
}

</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        @if(!empty($message))
        @if($message == 'ITEM_ADDED')
        showMessage(gks.successADD);
        @elseif($message == 'ITEM_EDITED')
        showMessage(gks.successEDIT);
        @elseif($message == 'ITEM_DELETED')
        showMessage(gks.successDEL);
        @elseif($message == 'ITEM_UPDATED')
        showMessage(gks.successUPDATE);
        @endif
        @endif
    });
</script>
@stop

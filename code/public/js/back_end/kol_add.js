jQuery(document).ready(function () {
    jQuery("#frm-add").on("submit", function (e) {
        e.preventDefault();
        submitFrm();
    });
});

function submitFrm() {
    if (isValidFrm()) {
        var frm = jQuery("#frm-add");
        frm.find("button").hide().after(gks.loadingIMG);
        frm[0].submit();
    }
    return false;
}

function isValidFrm() {
    var frm = jQuery("#frm-add");
    var frmName = frm.find("input[name=name]").val().trim();
    var frmRole = frm.find("input[name=role]").val().trim();
    var frmStar = frm.find("input[name=star]").val().trim();
    var frmNote = frm.find("textarea[name=note]").val().trim();
    var valid = true;

    frm.find(".alert").addClass("hidden");

    if (!frmName || frmName === "") {
        frm.find("#req-name .alert-danger").removeClass("hidden");
        valid = false;
    }
    if (!frmRole || frmRole === "") {
        frm.find("#req-role .alert-danger").removeClass("hidden");
        valid = false;
    }
    if (!frmNote || frmNote === "") {
        frm.find("#req-note .alert-danger").removeClass("hidden");
        valid = false;
    }

    if (
        !frmStar ||
        frmStar === "" ||
        parseInt(frmStar) > 5 ||
        parseInt(frmStar) < 1
    ) {
        frm.find("#req-star .alert-danger").removeClass("hidden");
        valid = false;
    }

    return valid;
}
